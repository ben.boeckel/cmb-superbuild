set(cmb_doc_dir "share/cmb/doc")
set(cmb_example_dir "examples")
set(plugin_dir "lib")

include(aeva.bundle.common)
set(cmb_package "aeva ${aeva_version_major}.${aeva_version_minor}.${aeva_version_patch}")
include(cmb.bundle.apple)

# Install PDF guides.
cmb_install_extra_data()
