# Capstone is a private program to which Kitware developers have access. We
# provide logic for situating these programs into our superbuild for development
# purposes only. Nothing is bundled or distributed.
superbuild_add_project(capstone
  CAN_USE_SYSTEM
  DOWNLOAD_NO_EXTRACT 1
  CONFIGURE_COMMAND
    /bin/sh -c "yes | hdiutil attach -mountpoint '<SOURCE_DIR>' '<DOWNLOADED_FILE>'"
  BUILD_COMMAND
    ${CMAKE_COMMAND} -E copy_directory "<SOURCE_DIR>/Capstone-9.1.2" "<BINARY_DIR>"
  INSTALL_COMMAND
    hdiutil detach <SOURCE_DIR>
)

set(capstone_pythonpath ${CMAKE_CURRENT_BINARY_DIR}/capstone/build/Capstone.app/Contents/MacOS)
set(CreateMG_DIR ${CMAKE_CURRENT_BINARY_DIR}/capstone/build/lib)
