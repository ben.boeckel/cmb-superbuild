# Cubit is a private program to which Kitware developers have access. We provide
# logic for situating these programs into our superbuild for development
# purposes only. Nothing is bundled or distributed.
superbuild_add_project(cubit
  CAN_USE_SYSTEM
  DOWNLOAD_NO_EXTRACT 1
  CONFIGURE_COMMAND
    /bin/sh -c "yes | hdiutil attach -mountpoint '<SOURCE_DIR>' '<DOWNLOADED_FILE>'"
  BUILD_COMMAND
    ${CMAKE_COMMAND} -E copy_directory "<SOURCE_DIR>/Cubit-15.2" "<BINARY_DIR>"
  INSTALL_COMMAND
    hdiutil detach <SOURCE_DIR>
)

set(CUBIT_EXE ${CMAKE_CURRENT_BINARY_DIR}/cubit/build/Cubit.app/Contents/MacOS/Cubit)
