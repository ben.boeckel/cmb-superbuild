if (DEFINED ENV{GITLAB_CI})
  # Inform ITK to allow all compilers to avoid clang-tidy errors:
  set(environment
    PROCESS_ENVIRONMENT ITK_ACCEPT_ALL_COMPILERS 1)
  message("Instructing ITK to accept all compilers")
endif()

superbuild_add_project(itk
  DEPENDS cxx11
  ${environment}
  CMAKE_ARGS
    # Build what you need
    -DBUILD_EXAMPLES:BOOL=OFF
    -DBUILD_TESTING:BOOL=OFF
    -DITK_ACCEPT_ALL_COMPILERS:BOOL=ON
    -DITK_BUILD_DEFAULT_MODULES:BOOL=ON
    -DITK_BUILD_DOCUMENTATION:BOOL=OFF
    -DITK_WRAP_PYTHON:BOOL=OFF
    -DITK_SKIP_PATH_LENGTH_CHECKS:BOOL=ON
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
)

set(itk_build_dir ${CMAKE_CURRENT_BINARY_DIR}/itk/build)
set(itk_install_dir <INSTALL_DIR>/lib/cmake/ITK-5.1)
