set(CPACK_PACKAGE_DESCRIPTION_SUMMARY
  "CMB aeva application")
set(CPACK_PACKAGE_NAME "AEVA")
set(cmb_package_name "aevaCMB")

set(cmb_programs_to_install
  aevaCMB
)

set(cmb_install_paraview_server FALSE)
set(cmb_install_paraview_python TRUE)

set(package_version_name "aeva")
set(package_suffix "${AEVA_PACKAGE_SUFFIX}")
include(aeva-version)

include(cmb.bundle.common)
