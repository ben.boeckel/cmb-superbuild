set(ENABLE_cmb "OFF" CACHE BOOL "")

if ("$ENV{CI_JOB_NAME}" MATCHES "centos7")
  # matplotlib is not necessary, but this is as good a place as any to test it
  set(ENABLE_matplotlib "ON" CACHE BOOL "")
else ()
  set(ENABLE_matplotlib "OFF" CACHE BOOL "")
endif ()

set(ENABLE_paraview "ON" CACHE BOOL "")
set(ENABLE_smtk "ON" CACHE BOOL "")
set(ENABLE_smtkprojectmanager "OFF" CACHE BOOL "")
set(ENABLE_smtkresourcemanagerstate "OFF" CACHE BOOL "")
