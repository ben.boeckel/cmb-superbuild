include("${CMAKE_CURRENT_LIST_DIR}/gitlab_ci.cmake")

# Read the files from the build directory.
ctest_read_custom_files("${CTEST_BINARY_DIRECTORY}")

# Pick up from where the configure left off.
ctest_start(APPEND)

ctest_build(
  RETURN_VALUE build_result)
ctest_submit(PARTS Build)

file(GLOB logs
  "${CTEST_BINARY_DIRECTORY}/superbuild/*/stamp/*-build-*.log"
  "${CTEST_BINARY_DIRECTORY}/superbuild/*/stamp/*-install-*.log")
if (logs)
  list(APPEND CTEST_NOTES_FILES ${logs})
  ctest_submit(PARTS Notes)
endif ()

if (build_result)
  message(FATAL_ERROR
    "Failed to build")
endif ()
