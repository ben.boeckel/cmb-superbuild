string(REPLACE "origin/" "" paraview_tag "${paraview_GIT_TAG}")
string(REPLACE ".git" "/raw/${paraview_tag}/version.txt" paraview_version_url "${paraview_GIT_REPOSITORY}")

# Download ParaView's master branch version.txt
file(DOWNLOAD "${paraview_version_url}"
  ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/paraviewVersion.txt STATUS status)
list(GET status 0 error_code)
if (error_code)
  message(FATAL_ERROR "Could not access the version file for ParaView")
endif ()

file(STRINGS ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/paraviewVersion.txt version_string )

string(REGEX MATCH "([0-9]+)\\.([0-9]+)\\.([0-9]+)[-]*(.*)"
  version_matches "${version_string}")

set(paraview_version_major ${CMAKE_MATCH_1})
set(paraview_version_minor ${CMAKE_MATCH_2})
set(paraview_version "${paraview_version_major}.${paraview_version_minor}")
if (CMAKE_MATCH_3)
  set(paraview_version_patch ${CMAKE_MATCH_3})
  set(paraview_version "${paraview_version}.${paraview_version_patch}")
else()
  set(paraview_version_patch 0)
endif()
# To be thorough, we should split the label into "-prerelease+metadata"
# and, if prerelease is specified, use it in determining precedence
# according to semantic versioning rules at http://semver.org/ .
# For now, just make the information available as a label:
if (CMAKE_MATCH_4)
  set(paraview_version_label "${CMAKE_MATCH_4}")
endif()
